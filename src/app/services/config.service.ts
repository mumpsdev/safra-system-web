import { Injectable } from '@angular/core';
import { TranslateService } from '../utils/translate/translate.service';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(
    private translate: TranslateService
  ) { }

  public getLocale(): string{
    return this.translate.getValue("LOCALE");
  }
}
