
export const LANG_ES_NAME = 'es';

export const LANG_ES_TRANS = {
    "BEM_VINDOS": "Bienvenidos",
    "PESQUISAR": "Búsqueda",
      //------------------------------------------------------------------------    Erros de stastus do servidor
    "statusDefault": "Error al intentar conectar con el servidor.",
    "status302": "El recurso se trasladó temporalmente a otro URI.",
    "status304": "No se ha cambiado el recurso.",
    "status400": "Los datos de la solicitud no son válidos.",
    "status401": "La URI especificada requiere autenticación del cliente. El cliente puede intentar realizar nuevas solicitudes.",
    "status403": "El servidor entiende la petición, pero se niega a atenderla. El cliente no debe intentar hacer una nueva solicitud.",
    "status404": "El servidor no encontró ninguna URI correspondiente.",
    "status405": "El método especificado en la solicitud no es válido en la URI. La respuesta debe incluir un encabezado Allow con una lista de los métodos aceptados.",
    "status410": "El recurso solicitado no está disponible pero no se conoce su dirección actual.",
    "status500": "El servidor no pudo completar la solicitud debido a un error inesperado.",
    "status502": "El servidor, mientras actuaba como proxy o gateway, recibió una respuesta no válida del servidor ascendente a la que hizo una petición.",
    "status503": "El servidor, mientras actuaba como proxy o gateway, recibió una respuesta no válida del servidor ascendente a la que hizo una petición.",
};