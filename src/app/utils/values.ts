import { NgxUiLoaderConfig } from 'ngx-ui-loader';

export const URLs = {//Urls rest.
    //---------------- Auth
   "AUTH_LOGIN": "/api/v1/login-auth/:login",
   "AUTH_AUTHENTICATE": "/api/v1/authenticate",
   "AUTH_ALL": "/*", 
   //---------------- Contact Type
   "CONTACT_TYPE": "/api/v1/contact-type",
   "CONTACT_TYPE_ID": "/api/v1/contact-type/:id"
}

export const typeMsg = {//Tipos de mensagens
   "DANGER": "msgErro",
   "SUCCESS": "msgSuccesso",
   "INFO": "msgInfo",
   "ALERT": "msgAlert"
}

export const objectMsg = {//Objetos de retorno
   "STATUS": "status",
   "ERROR": "error",
   "OBJ": "obj",
   "LIST": "list",
   "LIST_MSG": "listMsg",
   "PAGE": "page",
   "TOTAL_PAGES": "totalPages",
   "LIMIT": "limit",
   "RANGE_INIT": "rangeInit",
   "RANGE_END": "rangeEnd",
   "ASC": "asc",
   "DESC": "desc",
   "TOTAL_ROWS": "totalRows",
}

export const secretToken ={
   "TOKEN": "x-access-token",
   "SECRET_PUBLIC" : "projeto-publico"
}

export const ngxUiLoaderConfig: NgxUiLoaderConfig = {
   "bgsColor": "black",
   "bgsOpacity": 1,
   "bgsPosition": "center-center",
   "bgsSize": 200,
   "bgsType": "three-strings",
   "blur": 5,
   "fgsColor": "black",
   "fgsPosition": "center-center",
   "fgsSize": 120,
   "fgsType": "three-strings",
   "gap": 30,
   "logoPosition": "center-center",
   "logoSize": 80,
   "logoUrl": "assets/logotipomumps.png",
   "overlayColor": "rgba(0,0,0,0.4)",
   "pbColor": "black",
   "pbDirection": "ltr",
   "pbThickness": 3,
   "hasProgressBar": true,
   "textColor": "black",
   "textPosition": "center-center",
   "threshold": 500
 };