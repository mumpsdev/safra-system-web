import { 
  Component,
  OnInit,
  Input,
  Output,
  ElementRef,
  ViewChild,
  EventEmitter,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from '../../utils/translate/translate.service';

@Component({
  selector: 'm-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit{
  @Input()
  placeholder: string = "";
  @Input()
  maxlength: number;
  @Input()
  type: string  = "text";
  @Input()
  width: string  = "";
  @Input()
  icon: string  = "";
  @Input()
  ctrl: FormControl;
  @Output()
  onKeyUp: EventEmitter<any> = new EventEmitter();

  @Input("focus")
  private innerFocus = false;
  
  constructor(
    private translate: TranslateService
  ) { }

  iconPassword: string;

  ngOnInit() {
    if(!this.width){this.width = "100%"}
    if(this.placeholder){
      this.placeholder = this.translate.getValue(this.placeholder);
    }   
    //visibility_off' : 'visibility
    if(this.type == "password"){
      this.iconPassword = "visibility_off";
    }
  }
  
  showIconPassword(){
    if(this.iconPassword == "visibility_off"){
      this.iconPassword = "visibility"
      this.type = "text";
    }else{
      this.iconPassword = "visibility_off";
      this.type = "password";
    }
  }

  private keyUp(event){
    this.onKeyUp.emit(event);
  }

}
