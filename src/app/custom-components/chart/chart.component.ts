import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'm-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit{

  constructor() { }
  @Input() data: any;
  @Input() labels: Array<any>;
  @Input() options: any;
  @Input() showLegend: boolean = false;
  @Input() typeChart: string = "line";
  @Input() addClass: string = "";
  @Output() onclick: EventEmitter<any> = new EventEmitter();
  @Output() onHover: EventEmitter<any> = new EventEmitter();

  ngOnInit() {

    BaseChartDirective.defaultColors = [
      [156, 39, 176],
      [76, 175, 80],
      [103, 58, 183],
      [244, 67, 54],
      [205, 220, 57],
      [255, 235, 59],
      [63, 81, 181],
      [255, 152, 0],
      [96, 125, 139],
      [183, 28, 28],
      [1, 87, 155],
      [74, 20, 140],
      [0, 96, 100],
      [136, 14, 79],
      [49, 27, 146],
      [0, 77, 64],
      [62, 39, 35],
      [51, 105, 30]
    ];
  }

  public optionsChart() {
    return {
      responsive: true,
      segmentShowStroke: false,
      legend: {display: this.showLegend}
    }
  };
  

  public colors:Array<any> = [];

  // events
  public chartClicked(obj: any): void {
    this.onclick.emit(obj);
    console.log(obj);
  }
 
  public chartHovered(obj: any): void {
    this.onHover.emit(obj);
    console.log(obj);
  }
  resize(){
  }
}
