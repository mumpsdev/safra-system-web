import { formatCurrency, formatDate, formatNumber, getLocaleCurrencySymbol } from '@angular/common';
import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2, ViewChild } from '@angular/core';
import { LANG_PT_TRANS } from 'src/app/utils/translate/lang-pt';
import { TranslateService } from 'src/app/utils/translate/translate.service';
import { UtilsService } from '../../services/utils.service';
import { ngIfScaleIn, ngIfScaleOut, ngIfSlide } from '../../utils/animates.custons';
import { objectMsg } from '../../utils/values';
import { HeadTableComponent } from './head-table/head-table.component';
@Component({
  selector: 'm-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  animations:[ngIfScaleIn, ngIfScaleOut, ngIfSlide]
})
export class TableComponent implements OnInit, AfterViewInit {

  constructor(
    private UtilsService: UtilsService,
    private renderer: Renderer2,
    private translate: TranslateService
  ) { 
    this.heads = new Array();
  }

  // --------- Entradas
  @Input() list: Array<any>;
  @Input() addClass: string;
  @Input() titleClass: string;
  @Input() isMult: boolean = false;
  @Input() isSelected: boolean = false;
  @Input() size: string;
  @Input() height: string;
  @Input() headFixed: boolean = false;
  @Input() infinity: boolean = false;

  // --------- Saídas
  @Output() loadTable: EventEmitter<any> = new EventEmitter();
  @Output() onClick: EventEmitter<any> = new EventEmitter();
  @Output() ondbClick: EventEmitter<any> = new EventEmitter();

  @ViewChild("bodyTable") bodyTable: ElementRef;
  @ViewChild("divFixedRow") divFixedRow: ElementRef;
  private _data: any;
  private asc = [];
  private desc = [];
  public search = {page: 1, limit: 10, asc: "", desc: ""};
  public heads: Array<HeadTableComponent>;
  public itemSelected: any;
  public listSelected: any;
  private showShadowLeft: boolean = false;
  private showShadowRight: boolean = false;
  private onScroll: boolean = false;
  private headHeight = 0;
  private scrollHeadDown = false;
  private scrollTop = 0;
  private locale = this.translate.getValue(LANG_PT_TRANS.LOCALE);
  private symbolCurrency = getLocaleCurrencySymbol(this.locale);
  private dateFormat = this.translate.getValue(LANG_PT_TRANS.DATE_FORMAT);
  private limits = ["5", "10", "20", "30", "40", "50", "100"];
  private linePass = -1;
  private initFirst = false;
  


  ngOnInit() {
    setTimeout(() => {
      if(this.headFixed || this.height){
        this.setHeightDiv();
      }
      if(this.headFixed && !this.height || this.infinity){
        this.setListenerScrollPage();
      }
    }, 500);
  }
  
  ngAfterViewInit(){
  }

  @Input() set data(data){
    this.initFirst = true;
    setTimeout(() => {
      this.setHeightDiv();
    }, 300);
    // if(data && !this.initFirst){
    // }
    this._data = data;
  }

  get data(){
    return this._data;
  }
  public setHeightDiv(){
    let interval = setInterval(() => {
      try {
        this.headHeight = this.bodyTable.nativeElement.querySelector("thead tr").offsetHeight;
        if(this.headFixed || this.height){
          this.renderer.setStyle(this.divFixedRow.nativeElement, "min-height", `${this.headHeight}px`);
        }
        if(this.headHeight > 0){
          clearInterval(interval);
          this.scrollTable(this.bodyTable.nativeElement);
        }
        } catch (error) {}
      }, 100);
  }

  public addHeads(head: HeadTableComponent){
    this.heads.push(head);
  }
  
  get limit(){
    return this.data.limit || this.search.limit;
  }

  set limit(value){
    this.search.limit = value;
    this.data.limit = value;
  }
  
  setLimit(event){
    this.limit = event.value;
    if((this.data.page * this.data.limit) >= this.data.qtdTotal){
      this.data.page = 1;
      this.search.page = this.data.page;
    }
    this.emitirSearch();
  }
  
  pageNext(){
    this.search.page = this.data.page;
    this.search.page = ++this.search.page;
    this.emitirSearch();
  }
  
  pageBack(){
    this.search.page = this.data.page;
    this.search.page = --this.search.page;
    this.emitirSearch();
  }
  
  setSort(field:string){
    if(this.data && this.data.list && this.data.list.length > 1){
      if(this.asc.includes(field) && !this.desc.includes(field)){
        this.removeAsc(field);
        this.addDesc(field);
      }else if(!this.asc.includes(field) && this.desc.includes(field)){
        this.removeAsc(field);
        this.removeDesc(field);
      }else if(!this.asc.includes(field) && !this.desc.includes(field)){
        this.addAsc(field);
      }
      this.emitirSearch();
    }
  }

  setSortAscDesc(){
    this.search.asc = this.asc.toString();
    this.search.desc = this.desc.toString();
  }

  addAsc(head: string){
    if(!this.asc.includes(head)){
      this.asc.push(head);
    }
  }
  addDesc(head: string){
    if(!this.desc.includes(head)){
      this.desc.push(head);
    }
  }

  removeAsc(head: string){
    if(this.asc.includes(head)){
      this.asc.splice(this.asc.indexOf(head), 1);
    }
  }

  removeDesc(head: string){
    if(this.desc.includes(head)){
      this.desc.splice(this.desc.indexOf(head), 1);
    }
  }

  inSort(head: string){
    return this.asc.includes(head) || this.desc.includes(head);
  }

  inAsc(head: string){
    return this.asc.includes(head);
  }
  inDesc(head: string){
    return this.desc.includes(head);
  }

  
  async clickRow(item:any){
    if(this.isSelected){
      if(!this.isMult){
        if(this.itemSelected && this.itemSelected["selected"]){
          if(this.itemSelected == item){
            item.selected = false;
            this.itemSelected = null;
          }else{
            this.itemSelected["selected"] = false;
            item.selected = true;
            this.itemSelected = item;
          }
        }else{
          item.selected = true;
          this.itemSelected = item;
        }
        this.onClick.emit(this.itemSelected);
      }else{
        if(item["selected"]){
          item["selected"] = false;
        }else{
          item["selected"] = true;
        }
        this.listSelected = this.list || this.data[objectMsg.LIST];
        this.listSelected = this.listSelected.filter( (item) => {return item["selected"]});
        this.onClick.emit(this.listSelected);
      }
    }
  }

  reload(){
    this.emitirSearch()
  }
  
  emitirSearch(){
    if(this.data.limit){
      this.search["limit"] = this.data.limit;
    }
    this.setSortAscDesc();
    this.loadTable.emit(this.search);
  }

  getValueField(value:object, head:HeadTableComponent){
    if(value[head.field]){
      if(head.digitsNumber){
        return formatNumber(value[head.field], this.locale, head.digitsNumber)
      }else if(head.date){
        return formatDate(value[head.field], this.dateFormat, this.locale);
      }else if(head.currency){
        return formatCurrency(value[head.field], this.locale, head.showCurrencySymbol ? this.symbolCurrency: "");
      }else{
        return this.UtilsService.getValueObjectField(value, head.field);
      }
    }
  }

  clickBtn(head: HeadTableComponent, item: any){
    head.clickEmitter(item);
  }
  
  public scrollTable(target){
    let tolerance = 10;
    if(!this.onScroll){
      this.onScroll = true;
      if(target){
        let widthScroll = target.scrollLeft;
        let widthMax = target.scrollWidth - target.clientWidth;
        if(target.scrollLeft >= tolerance){
          this.showShadowLeft = true;
        }else if(target.scrollLeft < tolerance){
          this.showShadowLeft = false;
        }
        if((widthScroll + tolerance) <= widthMax){
          this.showShadowRight = true;
        }else{
          this.showShadowRight = false;
        }
      }
      if(target.scrollTop > tolerance){
        this.scrollHeadDown = true;
      }else{
        this.scrollHeadDown = false;
      }
      this.onScroll = false;
    }
    this.scrollTop = target.scrollTop;
    if(this.infinity){
      this.setVirtualScroll();
    }
    
    
  }

  setVirtualScroll(scrollY?){
    if(this.headHeight > 0){
      try {
          let scrollTop = this.scrollTop;
          let heightLine = this.bodyTable.nativeElement.querySelector("tbody tr:last-child").offsetHeight
          let heightVisible = this.bodyTable.nativeElement.offsetHeight - this.headHeight;
          let isEndScroll = false;
          if(!this.height){
            let heightTable = this.bodyTable.nativeElement.querySelector("table").offsetHeight
            let top = ((this.bodyTable.nativeElement.getBoundingClientRect().top + this.headHeight) * -1);
            scrollTop = top;
            console.log(`heightVisible 1: ${heightVisible}`);
            if(top < 0){
              heightVisible = window.innerHeight + top;
              scrollTop = 0;
            }else{
              heightVisible = window.innerHeight;
            }
            if((window.document.scrollingElement.clientHeight + scrollY) == window.document.scrollingElement.scrollHeight){
              if(heightTable > (heightVisible + top)){
                isEndScroll = true;}else{isEndScroll = false;
              }
            }
          }
          let linePassNow = Math.round(scrollTop  / heightLine) > 0 ? Math.round(scrollTop  / heightLine) : 0;
          if(linePassNow != this.linePass || isEndScroll){
            if(linePassNow != 0){
            }
            this.linePass = linePassNow;
            let qtdVisible =  Math.round(heightVisible / heightLine) | 0;
            if((this.linePass + qtdVisible) >= this.limit || isEndScroll){
              this.limit = parseInt(this.limit) +  qtdVisible;
              this.emitirSearch();
            }
          }
        } catch (error) {}
      }
  }

  async resizeBody(){
    let platformNow: boolean = this.infinity = await this.UtilsService.isPlatformMobile();
    if(this.headHeight > 0){
      if(platformNow != this.infinity){
        this.infinity = platformNow;
      }
    }
    this.scrollTable(this.bodyTable);
  }

  setListenerScrollPage(){
    this.UtilsService.scrollY.subscribe( newY => {
      if(this.headFixed){
        let headHeight = this.bodyTable.nativeElement.offsetHeight - this.headHeight;
        let top = (this.bodyTable.nativeElement.getBoundingClientRect().top * -1);
        if(top > 0 && (top + this.headHeight) < headHeight){
          this.scrollTop =  top;
          this.scrollHeadDown = true;
        }else if(top < 0){
          this.scrollTop = 0;
          this.scrollHeadDown = false;
        }
      }
      if(this.infinity){
        this.setVirtualScroll(newY);
      }
    });
  }

  notRecord(e){
    return !this.data || (this.data && !this.data.list || this.data && this.data.list.length <= 0) 
      && (!this.list || this.list && this.list.length <= 0);
  }
}
