import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'm-fast-info',
  templateUrl: './fast-info.component.html',
  styleUrls: ['./fast-info.component.scss']
})
export class FastInfoComponent implements OnInit {
  @Input() icon: string;
  @Input() title: string;
  @Input() label: string;
  @Input() addClass: string;
  constructor() { }

  ngOnInit() {
  }

}
