import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListItemMenuLeftComponent } from './list-item-menu-left.component';

describe('ListItemMenuLeftComponent', () => {
  let component: ListItemMenuLeftComponent;
  let fixture: ComponentFixture<ListItemMenuLeftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListItemMenuLeftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListItemMenuLeftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
