import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { AuthGuard } from './services/auth.guard';
import { ContactTypeComponent } from './pages/contact-type/contact-type.component';
// import { AcaoComponent } from './pages/acao/acao.component';

const routes: Routes = [
  //Rotas principais
  { path: 'login', component: LoginComponent},
  { path: '', component: HomeComponent, canActivate: [AuthGuard]},
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  { path: 'contact-type', component: ContactTypeComponent, canActivate: [AuthGuard]},
  { path: 'contact-type-card', component: ContactTypeComponent, canActivate: [AuthGuard]},
  { path: '**', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
