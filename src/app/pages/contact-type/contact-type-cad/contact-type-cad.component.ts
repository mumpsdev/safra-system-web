import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { UtilsService } from 'src/app/services/utils.service';
import { LANG_PT_TRANS } from 'src/app/utils/translate/lang-pt';
import Validation from 'src/app/utils/Validation';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ContactTypeService } from 'src/app/services/contact.type.service';
import { TranslateService } from 'src/app/utils/translate/translate.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-contact-type-cad',
  templateUrl: './contact-type-cad.component.html',
  styleUrls: ['./contact-type-cad.component.scss']
})
export class ContactTypeCadComponent implements OnInit {

  private title = `${this.transalte.getValue(LANG_PT_TRANS.REGISTER)} ${this.transalte.getValue(LANG_PT_TRANS.CONTACT_TYPE)}`; 

  //Controles
  formContactType: FormGroup;
  id = new FormControl("");
  name = new FormControl("",
    this.utilsService.createValidator(Validation.isEmpty , LANG_PT_TRANS.REQUIRED_FIELD)
  );

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private utilsService: UtilsService,
    private contactTypeService: ContactTypeService,
    private transalte: TranslateService,
    private formbuilder: FormBuilder,
    private dialogRef: MatDialogRef<ContactTypeCadComponent>
  ) { 
    this.formContactType = this.formbuilder.group({
      "id": this.id,
      "name": this.name
    });
  }

  ngOnInit() {
    if(this.data){
      console.log(this.data);
      this.title = `${this.transalte.getValue(LANG_PT_TRANS.EDIT)} ${this.transalte.getValue(LANG_PT_TRANS.CONTACT_TYPE)}`; 
      this.formContactType.patchValue(this.data);
    }
  }

  async save(){
    this.contactTypeService.create(this.formContactType.value).pipe(take(1)).subscribe( data => {
      this.utilsService.showMesseges(data);
      this.dialogRef.close("ok");
    }, error => this.utilsService.showMesseges(error));
  }
  
  async edit(){
    this.contactTypeService.update(this.formContactType.value).pipe(take(1)).subscribe( data => {
      this.utilsService.showMesseges(data);
      this.dialogRef.close("ok");
    }, error => this.utilsService.showMesseges(error));
  }



}
